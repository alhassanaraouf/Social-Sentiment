import re
# from enchant.checker import SpellChecker
from dateutil import parser
from textblob import TextBlob
from nltk.corpus import stopwords

class Cleaning:
    def __init__(self):
        pass

    def aspell_correction(self, text, filters = "html"):
        """ Take english text and Correct all the spelling mistakes and return the resualt  """
        import aspell
        # client = sqlite3.connect("dict/bagofwords.db")
        # db = client.cursor()
        # temp = db.execute("SELECT * FROM words").fetchall()
        spell = aspell.Speller(('lang', 'en'), ("filter", filters), ("run-together", 'true'), ("validate-words", 'false'), ('encoding', 'utf-8'))
        # with open("dict/privet_dict.txt") as f:
        # content = f.readlines()
        # for x in content:
        # spell.add(x.strip())
        # for x in temp:
        # spell.add(x[1])
        temp = text.split()
        for i, word in enumerate(temp):
            word = word.encode('utf-8')
            if len(spell.suggest(word)) > 0:
                temp[i] = spell.suggest(word)[0]
        return " ".join(temp)

	
    def hunspell_correction(self, text):
        """ Take english text and Correct all the spelling mistakes and return the resualt  """
        from hunspell import Hunspell
        # client = sqlite3.connect("dict/bagofwords.db")
        # db = client.cursor()
        # temp = db.execute("SELECT * FROM words").fetchall()
        h = Hunspell(disk_cache_dir = ".cache")
        # with open("dict/privet_dict.txt") as f:
        # content = f.readlines()
        # for x in content:
        # spell.add(x.strip())
        # for x in temp:
        # spell.add(x[1])
        temp = text.split()
        su = h.bulk_suggest(temp)
        counter = 0
        for t in su:
            temp[counter] = su[t][0]
            counter = counter + 1
        return " ".join(temp)


    def sentence_tokens(self, text):
        doc = TextBlob(text)
        x = doc.sentences
        return x


    def preprocess(self, text, spell_program="aspell"):
        """ fix the text for spllings and remove all unnecessary things """
        text = text.lower()
        # Remove usernames:
        text = re.sub("@[^\s]+", "", text)
        # Remove Hashtag Chars.
        hashtag_re = re.compile("([#][\w_-]+)")
        hashtags = hashtag_re.findall(text)
        if len(hashtags) > 0:
            for x in hashtags:
                temp = x
                temp = temp.replace("#", "").replace("_", " ").replace("-", " ")
                text = text.replace(x, temp)
        # Remove URLs
        text = re.sub(
            r"""(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))""",
            " ",
            text,
        )
        if spell_program == "hunspell":
            spell_correction = self.hunspell_correction
        elif spell_program == "aspell":
            spell_correction = self.aspell_correction
        stop = stopwords.words('english')
        temp = {'sentences': []}
        temp['text'] = text
        sentences = self.sentence_tokens(text)
        for x in sentences:
            temp2 = {}
            y = spell_correction(x)
            y = TextBlob(y)
            score = y.sentiment.polarity
            y = y.split()
            filtered_words = [word for word in y if word.lower() not in stop]
            y = " ".join(filtered_words)
            temp2[y] = score
            temp['sentences'].append(temp2)
        return temp

    def time_processing(self, time):
        """ tranfrorm the twitter data and time format to format a7sn  """
        s = parser.parse(time)
        return str(s)



