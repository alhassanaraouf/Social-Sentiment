import sqlite3
import os

class Aspects:
    def __init__(self):
        "docstring"
        pass

    def uploadfile(self):
        """
        Parse The Keywords Org File and add them to the sqlite database in same structure 
        """
        import orgparse as org
        import aspell
        import string

        s = aspell.Speller(('lang', 'en'),("validate-words", 'false'))
        if os.path.exists("dict/bagofwords.db"):
            os.remove("dict/bagofwords.db")
        client = sqlite3.connect("dict/bagofwords.db")
        db = client.cursor()
        db.execute("""CREATE TABLE words (keyword text, parent text)""")
        data = org.load("dict/bagofwords.org")
        for x in data[1:]:
            temp = {}
            temp["keyword"] = x.heading.lower()
            words = temp["keyword"].split()
            for item in words:
                item = item.strip(string.punctuation)
                s.addtoPersonal(item)
            if x.parent is data:
                temp["parent"] = ""
            else:
                temp["parent"] = x.parent.heading.lower()
            db.execute(
                "INSERT INTO words VALUES (?,?)",
                (temp["keyword"], temp["parent"]),
            )
        client.commit()
        client.close()
        s.saveAllwords()

    def getRelated(self, keyword):
        """
        Take Keyword to search for it in keywords database and return the keywords and the category it belongs to
        """
        client = sqlite3.connect("dict/bagofwords.db")
        db = client.cursor()
        keyword = keyword.lower()
        keys = []
        keys.append(keyword)
        h = keyword
        temp = db.execute("SELECT * FROM words WHERE keyword=?", (h,)).fetchone()
        if temp is None:
            return
        while temp[1] != "":
            temp = db.execute("SELECT * FROM words WHERE keyword=?", (temp[1],))
            temp = temp.fetchone()
            keys.append(temp[0])
        return keys
