from configparser import ConfigParser


_config = None


def load_config():
    global _config
    _config = ConfigParser()
    _config.read_file(open("config.ini"))


load_config()
