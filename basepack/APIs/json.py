from . import _config
from ..preprocessing import Cleaning
from ..sentiment import Sentiment
import json


def get_data(filepath):
    cleandata = []
    cleaning = Cleaning()
    with open(filepath, "r") as json_file:
        rawdata = json.load(json_file)
        for row in rawdata:
            if "text" in row:
                temp = {}
                temp["text"] = cleaning.preprocess(row["text"])
                temp["sentiment"] = Sentiment.polarity_scores(temp["text"])
                cleandata.append(temp)
    return cleandata
