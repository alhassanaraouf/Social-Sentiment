from . import _config
from ..preprocessing import Cleaning
from ..sentiment import Sentiment
import csv

# def readCsv(filepath, delimiter=","):
# data = []
# with open(filepath, mode='r') as csv_file:
# reader = csv.DictReader(csv_file, delimiter=delimiter)
# if "text" in reader.fieldnames:
# temp = {}
# for row in reader:
# temp["text"] = row["text"]
# data.append(temp)
# return data
# else:
# print("no text column")


def get_data(filepath, delimiter=","):
    cleaning = Cleaning()
    cleandata = []
    with open(filepath, mode="r") as csv_file:
        reader = csv.DictReader(csv_file, delimiter=delimiter)
        if "text" in reader.fieldnames:
            for row in reader:
                temp = {}
                temp["text"] = cleaning.preprocess(row["text"])
                temp["sentiment"] = Sentiment.polarity_scores(temp["text"])

                cleandata.append(temp)
            return cleandata
        else:
            print("no text column")
