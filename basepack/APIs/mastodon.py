import requests
from . import _config
from .auth import BearerAuth
from ..preprocessing import Cleaning
from .. import sentiment
import os

SCOPES = "read"


class MastodonApi:
    """ Functions for Fetching The Data Form Twitter  """


    def get_token(self):
        if os.path.exists(".cache/mastodon"): 
            with open(".cache/mastodon", "r") as keyfile:
                key = keyfile.readline().strip()
            return key
        else:
            client_key = _config.get("mastodon", "client_key", raw=True)
            client_secret = _config.get("mastodon", "client_secret", raw=True)
            redirect_uri = _config.get("mastodon", "redirect_uri", raw=True)
            username = _config.get("mastodon", "username", raw=True)
            password = _config.get("mastodon", "password", raw=True)
            params = {
                "client_id": client_key,
                "client_secret": client_secret,
                "grant_type": "password",
                "username": username,
                "password": password,
            }
            res = requests.post(self.base + "oauth/token", params=params)
            key = res.json()["access_token"]
            with open('.cache/mastodon', 'w') as keyfile:
                keyfile.write(key)
            return key
    # def getToken():
    # client_key = _config.get("mastodon", "client_key")
    # client_secret = _config.get("mastodon", "client_secret")
    # redirect_uri = _config.get("mastodon", "redirect_uri")

    # def login(username, password):

    # params = {
    # 'grant_type': 'password',
    # 'client_id': app.client_id,
    # 'client_secret': app.client_secret,
    # 'username': username,
    # 'password': password,
    # 'scope': SCOPES,
    # }
    #
    # res = requests.post(self.base + "oauth/token", params=params)
    #
    # # If auth fails, it redirects to the login page
    # if res.is_redirect:
    # raise AuthenticationError()
    #
    # return res.json()

    def __init__(self):
        """
         the class take bearerkey (Optionaly, if not provided the defualt one will be used)
        """
        self.base = "https://" + _config.get("mastodon", "instance") + "/"
        bearerkey = self.get_token()
        self.auth = BearerAuth(bearerkey)

    def verify(self):
        endpoint = "api/v1/apps/verify_credentials"
        url = self.base + endpoint
        res = requests.get(url, auth=self.auth)
        return res.json()

    def getBookmark(self):
        endpoint = "api/v1/bookmarks"
        url = self.base + endpoint
        res = requests.get(url, auth=self.auth)
        return res.json()

    def Search(self, quary, NumberOfPages=3, lang="en"):
        """ Search twitter with the Query Provided 
            take other two optionaly arguments
            NumberOfPages: The Page is 100 Tweets
            lang: Laguages of the tweet returned
         """
        PageCounter = 0
        params = {"q": quary, "limit": 40}
        api = "api/v2/search"
        url = self.base + api
        res = requests.get(url, params=params, auth=self.auth)
        hashtag = res.json()["hashtags"][0]["name"]
        api = "/api/v1/timelines/tag/"+ hashtag
        url = self.base + api
        params = {"limit": 40}
        tweets = []
        while PageCounter < NumberOfPages:
            PageCounter += 1
            res = requests.get(url, params=params, auth=self.auth)
            res_json = res.json()
            ids = [i["id"] for i in res_json]
            if not ids:
                break
            params["max_id"] = int(min(ids)) - 1
            tweets = tweets + res_json
        return tweets


def get_data(keyword):
    numberOfRequests = _config.getint("mastodon", "requests")
    Mastodon = MastodonApi()
    cleaning = Cleaning()
    rawdata = Mastodon.Search(keyword, numberOfRequests)
    cleandata = []
    temp = {}
    for toot in rawdata:
        temp["id"] = toot["id"]
        temp["text"] = cleaning.preprocess(toot["content"])
        temp["sentiment"] = sentiment.analysis(temp["text"])
        temp["created_at"] = toot["created_at"]
        cleandata.append(temp)
    return cleandata

