import requests  # Library Used to send requests to Twitter Server
from . import _config
from .auth import BearerAuth
from ..preprocessing import Cleaning
from .. import sentiment
import pprint


class TwitterApi:
    """ Functions for Fetching The Data Form Twitter  """

    def __init__(self):
        """
         the class take bearerkey (Optionaly, if not provided the defualt one will be used)
        """
        bearerkey = _config.get("twitter", "bearer", raw=True)
        self.auth = BearerAuth(bearerkey)
        self.base = "https://api.twitter.com/1.1/"

    def Search(self, quary, NumberOfPages=3, lang="en"):
        """ Search twitter with the Query Provided 
            take other two optionaly arguments
            NumberOfPages: The Page is 100 Tweets
            lang: Laguages of the tweet returned
         """
        PageCounter = 0
        params = {"q": quary, "count": 100, "lang": lang, "resulty_type": "recent"}
        api = "search/tweets.json"
        url = self.base + api
        tweets = []
        while PageCounter < NumberOfPages:
            PageCounter += 1
            res = requests.get(url, params=params, auth=self.auth)
            res_json = res.json()["statuses"]
            ids = [i["id"] for i in res_json]
            params["max_id"] = min(ids) - 1
            tweets = tweets + res_json
        return tweets


def get_data(keyword):
    numberOfRequests = _config.getint("twitter", "requests")
    Twitter = TwitterApi()
    cleaning = Cleaning()
    rawdata = Twitter.Search(keyword, numberOfRequests)
    cleandata = []
    temp = {}
    for tweet in rawdata:
        temp["id"] = tweet["id_str"]
        temp["text"] = cleaning.preprocess(tweet["text"])
        temp["created_at"] = cleaning.time_processing(tweet["created_at"])
        temp["sentiment"] = sentiment.analysis(temp["text"]["text"])
        temp["source"] = "Twitter"
        print(temp)
        cleandata.append(temp)
    return cleandata
